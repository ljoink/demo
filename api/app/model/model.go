package model

import (
	// "app/utils"
	"database/sql"

	"ljoink/api-demo/service"
	"ljoink/api-demo/util"
)

const (
	host     = "localhost"
	port     = 5432
	user     = "root"
	password = "pw"
	dbname   = "demo"
)

var db *sql.DB

func init() {
	util.WriteLog("Model init...", "normal")
	db = service.Db.GetConnection()
	initDb()
}

func initDb() {
	customFun := `
		CREATE OR REPLACE FUNCTION trigger_set_timestamp() 
			RETURNS TRIGGER AS $$
			BEGIN
				NEW.updated_at = NOW();
				RETURN NEW;
			END;
		$$ LANGUAGE plpgsql;
	`
	if _, err := db.Exec(customFun); err != nil {
		util.WriteLog(err.Error(), "error")
		panic(err.Error())
	}

	adminSql := `
		CREATE TABLE admin (
			id serial PRIMARY KEY,
			email VARCHAR ( 255 ) UNIQUE NOT NULL,
			password VARCHAR ( 255 ) NOT NULL,
			phone VARCHAR ( 32 ) NOT NULL,
			address VARCHAR ( 500 ) NOT NULL
		)
	`
	if _, err := db.Exec(adminSql); err != nil {
		if err.Error() != "pq: relation \"admin\" already exists" {
			util.WriteLog(err.Error(), "error")
			panic(err.Error())
		}
	}

	itemSql := `
		CREATE TABLE item (
			id serial PRIMARY KEY,
			name VARCHAR ( 255 ) NOT NULL,
			description VARCHAR ( 255 ) NOT NULL,
			created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
			created_by INT NOT NULL,
			updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
		);
		CREATE TRIGGER set_timestamp
			BEFORE UPDATE ON item
			FOR EACH ROW
			EXECUTE PROCEDURE trigger_set_timestamp();
	`
	if _, err := db.Exec(itemSql); err != nil {
		if err.Error() != "pq: relation \"item\" already exists" {
			util.WriteLog(err.Error(), "error")
			panic(err.Error())
		}
	}
}

func checkDb() {
	db = service.Db.PingWithReconnect()
}
