package model

type Admin struct {
	Id       int
	Email    string
	Password string
	Phone    string
	address  string
}

type AdminCreator struct {
	Email    string `form:"email" json:"email" binding:"required,email"`
	Password string `form:"password" json:"password" binding:"required,min=8"`
	Phone    string `form:"phone" json:"phone" binding:"required,min=8,max=32"`
	Address  string `form:"address" json:"address" binding:"max=500"`
}

// return id
func (m *AdminCreator) Exec() (int, error) {
	checkDb()

	sql := `
		INSERT INTO admin
			(email, password, phone, address)
			VALUES ($1, $2, $3, $4)
			RETURNING id
	`

	var id int
	return id, db.QueryRow(sql, m.Email, m.Password, m.Phone, m.Address).Scan(&id)
}

type AdminLogin struct {
	Email    string `form:"email" json:"email" binding:"required"`
	Password string `form:"password" json:"password" binding:"required"`
}

func (m *AdminLogin) Exec() (Admin, error) {
	checkDb()

	sql := `
		SELECT * FROM admin
			WHERE email=$1
	`

	var admin Admin
	return admin, db.QueryRow(sql, m.Email).Scan(&admin.Id, &admin.Email, &admin.Password, &admin.Phone, &admin.address)
}
