package model

import (
	"fmt"
	"ljoink/api-demo/util"
	"strconv"

	"github.com/lib/pq"
)

type Item struct {
	Id          int
	Name        string
	Description string
	CreatedAt   pq.NullTime
	CreatedBy   int
	UpdatedAt   pq.NullTime
}

func (m *Item) Exec(id int) error {
	checkDb()

	sql := `
		SELECT * FROM item
			WHERE id=$1
	`
	return db.QueryRow(sql, id).Scan(
		&m.Id,
		&m.Name,
		&m.Description,
		&m.CreatedAt,
		&m.CreatedBy,
		&m.UpdatedAt,
	)
}

type ItemSelect struct {
	Page string `form:"page" binding:"integer"`
	From string `form:"from" binding:"date"`
	To   string `form:"to" binding:"date,dateAfter=From"`
}

func (m *ItemSelect) Exec() []Item {
	checkDb()

	where := ""
	if m.From != "" && m.To != "" {
		where = fmt.Sprintf("WHERE created_at BETWEEN '%s' AND '%s'", m.From, m.To)
	}
	util.WriteLog(where, "debug")

	limit := 3
	page, _ := strconv.Atoi(m.Page)
	offset := (limit * page) - limit

	sql := fmt.Sprintf("SELECT * FROM item %s LIMIT $1 OFFSET $2", where)

	rows, err := db.Query(sql, limit, offset)
	if err != nil {
		panic(err)
	}

	var itemArr []Item
	for rows.Next() {
		item := Item{}
		err := rows.Scan(
			&item.Id,
			&item.Name,
			&item.Description,
			&item.CreatedAt,
			&item.CreatedBy,
			&item.UpdatedAt,
		)
		if err != nil {
			panic(err)
		}

		itemArr = append(itemArr, item)
	}

	rows.Close()

	return itemArr
}

type ItemCreate struct {
	Name        string `form:"name" json:"name" binding:"required,max=255"`
	Description string `form:"description" json:"description" binding:"max=255"`
	CreatedBy   float64
}

func (m *ItemCreate) Exec() (int, error) {
	checkDb()

	sql := `
		INSERT INTO item
			(name, description, created_by)
			VALUES ($1, $2, $3)
			RETURNING id
	`

	var id int
	return id, db.QueryRow(sql, m.Name, m.Description, m.CreatedBy).Scan(&id)
}
