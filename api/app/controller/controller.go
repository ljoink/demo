package controller

import (
	"ljoink/api-demo/util"
	"net/http"

	jwt_go "github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
	"golang.org/x/crypto/bcrypt"
)

var Admin = &adminStruct{}
var Info = &infoStruct{}
var Item = &itemStruct{}

type ResponseStruct struct {
	Status  int
	Message string
	Result  interface{}
}

func Response(c *gin.Context, response *ResponseStruct) {
	if response.Status == 0 {
		response.Status = http.StatusOK
	}
	c.JSON(response.Status, gin.H{
		"message": response.Message,
		"result":  response.Result,
	})
}

func bind(c *gin.Context, jsonPtr interface{}) bool {
	if util.Reflect.Typeof(jsonPtr) != "ptr" {
		panic("jsonPtr should be ptr")
	}

	if err := c.ShouldBind(jsonPtr); err != nil {
		Response(c, &ResponseStruct{
			Status:  http.StatusBadRequest,
			Message: err.Error(),
		})
		return false
	}

	return true
}

func hash(str string) string {
	buffer, err := bcrypt.GenerateFromPassword([]byte(str), 10)
	if err != nil {
		panic(err.Error())
	}

	return string(buffer)
}

func checkHashedPassword(hashedPassword string, password string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))
	return err == nil
}

func genJWT(claims jwt_go.MapClaims) string {
	jwt := jwt_go.NewWithClaims(jwt_go.SigningMethodHS256, claims)
	key := []byte(util.GetEnv("jwtSecert"))

	token, err := jwt.SignedString(key)
	if err != nil {
		panic(err.Error())
	}

	return token
}
