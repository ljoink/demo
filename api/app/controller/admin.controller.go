package controller

import (
	"database/sql"
	"ljoink/api-demo/model"
	"time"

	jwt_go "github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

type adminStruct struct{}

func (*adminStruct) Create(c *gin.Context) {
	m := model.AdminCreator{}
	if ok := bind(c, &m); !ok {
		return
	}

	m.Password = hash(m.Password)

	id, err := m.Exec()
	if err != nil {
		if err.Error() == "pq: duplicate key value violates unique constraint \"admin_email_key\"" {
			Response(c, &ResponseStruct{
				Status:  400,
				Message: "Email exist",
			})
			return
		}

		panic(err.Error())
	}

	Response(c, &ResponseStruct{
		Message: "Admin created",
		Result:  id,
	})
}

func (*adminStruct) Login(c *gin.Context) {
	m := model.AdminLogin{}
	if ok := bind(c, &m); !ok {
		return
	}

	admin, err := m.Exec()

	var isInvalid bool
	if err != nil {
		if err == sql.ErrNoRows {
			isInvalid = true
		} else {
			panic(err.Error())
		}
	}

	isInvalid = !checkHashedPassword(admin.Password, m.Password)

	if isInvalid {
		Response(c, &ResponseStruct{
			Status:  400,
			Message: "Invalid email/password",
		})
		return
	}

	now := time.Now().Unix()
	jwt := genJWT(jwt_go.MapClaims{
		"id":   admin.Id,
		"role": "admin",
		"iss":  "demo",
		"iat":  now,
		"exp":  now + 900,
	})

	Response(c, &ResponseStruct{
		Result: jwt,
	})
}
