package controller

import (
	"github.com/gin-gonic/gin"
)

type infoStruct struct{}

func (*infoStruct) Version(c *gin.Context) {
	Response(c, &ResponseStruct{
		Result: "v1.0.0",
	})
}
