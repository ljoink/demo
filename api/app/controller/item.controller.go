package controller

import (
	"database/sql"
	"ljoink/api-demo/model"
	"net/http"
	"strconv"

	jwt_go "github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

type itemStruct struct{}

func (*itemStruct) Create(c *gin.Context) {
	claims, ok := c.MustGet("claims").(jwt_go.MapClaims)
	if !ok {
		panic("Missing claims")
	}

	m := model.ItemCreate{}
	if ok := bind(c, &m); !ok {
		return
	}
	m.CreatedBy = claims["id"].(float64)

	id, err := m.Exec()
	if err != nil {
		panic(err.Error())
	}

	Response(c, &ResponseStruct{
		Message: "Item created",
		Result:  id,
	})
}

func (*itemStruct) Read(c *gin.Context) {
	_, ok := c.MustGet("claims").(jwt_go.MapClaims)
	if !ok {
		panic("Missing claims")
	}

	m := &model.ItemSelect{}
	if err := c.ShouldBindQuery(m); err != nil {
		Response(c, &ResponseStruct{
			Status:  http.StatusBadRequest,
			Message: err.Error(),
		})
		return
	}

	Response(c, &ResponseStruct{
		Result: m.Exec(),
	})
}

func (*itemStruct) ReadCertain(c *gin.Context) {
	_, ok := c.MustGet("claims").(jwt_go.MapClaims)
	if !ok {
		panic("Missing claims")
	}

	id := c.Param("id")

	idInt, err := strconv.Atoi(id)
	if err != nil {
		Response(c, &ResponseStruct{
			Status:  http.StatusBadRequest,
			Message: "Invalid id",
		})
		return
	}

	m := model.Item{}
	if err := m.Exec(idInt); err != nil {
		if err == sql.ErrNoRows {
			Response(c, &ResponseStruct{
				Status:  http.StatusBadRequest,
				Message: "Data not found",
			})
			return
		}
		panic(err)
	}

	Response(c, &ResponseStruct{
		Result: m,
	})
}
