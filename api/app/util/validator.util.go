package util

import (
	"strconv"
	"time"

	"github.com/go-playground/validator/v10"
)

type validatorStruct struct{}

func (*validatorStruct) Integer(fl validator.FieldLevel) bool {
	if fl.Field().String() == "" {
		return true
	}
	_, err := strconv.Atoi(fl.Field().String())
	return err == nil
}

func (*validatorStruct) Date(fl validator.FieldLevel) bool {
	if fl.Field().String() == "" {
		return true
	}
	_, err := time.ParseInLocation("2006-01-02", fl.Field().String(), time.UTC)
	return err == nil
}

func (*validatorStruct) DateAfter(fl validator.FieldLevel) bool {
	value := fl.Field().String()

	if value == "" {
		return true
	}

	valueDate, err := time.ParseInLocation("2006-01-02", value, time.UTC)
	if err != nil {
		return false
	}

	compare := fl.Parent().FieldByName(fl.Param()).String()
	compareDate, err := time.ParseInLocation("2006-01-02", compare, time.UTC)
	if err != nil {
		return false
	}

	if valueDate.Before(compareDate) {
		return false
	}

	return true
}
