package util

import "reflect"

type reflectStruct struct{}

func (*reflectStruct) Typeof(any interface{}) string {
	return reflect.TypeOf(any).Kind().String()
}
