package util

import (
	"fmt"
	"log"
	"os"
	"runtime"
	"time"
)

var Reflect = &reflectStruct{}
var Validator = &validatorStruct{}

func WriteLog(any interface{}, prefix string) {
	//TODO: implement enum for prefix
	last := prefix[len(prefix)-1:]
	if last != "/" && last != "_" {
		prefix = prefix + "_"
	}

	currentTime := time.Now()
	path := "../log/" + prefix + currentTime.Format("20060102") + ".log"
	f, _ := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer f.Close()

	log.SetOutput(f)

	if _, file, line, ok := runtime.Caller(1); ok {
		log.Println(fmt.Sprintf("***From: %s:%d", file, line))
	} else {
		log.Println("*** ---")
	}
	log.Print(any)
	log.Print("\n\n")
}

func GetEnv(key string) string {
	// TODO: get env in production stage
	switch key {
	case "jwtSecret":
		return "p=A!o50oj_T[u6Eu>.B!h1@2~03DS`1}.kB#$</XOS}Lk"
	default:
		return ""
	}
}
