package middleware

import (
	"ljoink/api-demo/controller"
	"ljoink/api-demo/util"
	"net/http"
	"strings"

	jwt_go "github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

type jwtStruct struct{}

func (*jwtStruct) Check(c *gin.Context) {
	reqToken := c.Request.Header.Get("Authorization")
	if reqToken == "" {
		controller.Response(c, &controller.ResponseStruct{
			Status:  http.StatusBadRequest,
			Message: "Missing Token",
		})
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	splitToken := strings.Split(reqToken, "Bearer ")
	jwt := splitToken[1]

	if jwt == "" {
		controller.Response(c, &controller.ResponseStruct{
			Status:  http.StatusBadRequest,
			Message: "Invalid token",
		})
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	token, err := jwt_go.Parse(jwt, func(token *jwt_go.Token) (interface{}, error) {
		return []byte(util.GetEnv("jwtSecert")), nil
	})
	if err != nil {
		if err.Error() == "Token is expired" {
			controller.Response(c, &controller.ResponseStruct{
				Status:  http.StatusUnauthorized,
				Message: err.Error(),
			})
			c.AbortWithStatus(http.StatusInternalServerError)
			return
		}

		panic(err.Error())
	}

	if token.Valid {
		c.Set("claims", token.Claims.(jwt_go.MapClaims))
		c.Next()
		return
	}

	controller.Response(c, &controller.ResponseStruct{
		Status:  http.StatusUnauthorized,
		Message: "Token Invalid",
	})
	c.AbortWithStatus(http.StatusInternalServerError)
	return
}
