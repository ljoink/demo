package middleware

import (
	"ljoink/api-demo/controller"
	"ljoink/api-demo/util"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
)

type errorStruct struct{}

func (*errorStruct) Exec() gin.HandlerFunc {
	errlogfile, _ := os.Create("../log/error.log")
	gin.DefaultErrorWriter = errlogfile

	return gin.CustomRecovery(func(c *gin.Context, err interface{}) {
		util.WriteLog(err, "error")

		status := http.StatusInternalServerError
		if util.Reflect.Typeof(err) == "string" {
			controller.Response(c, &controller.ResponseStruct{
				Status:  status,
				Message: err.(string),
			})
		}

		c.AbortWithStatus(status)
		return
	})
}
