package main

import (
	"ljoink/api-demo/controller"
	"ljoink/api-demo/middleware"
	"ljoink/api-demo/util"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/validator/v10"
)

func router(r *gin.Engine) {
	r.GET("/info/version", controller.Info.Version)

	admin := r.Group("/admin")
	{
		admin.POST("/login", controller.Admin.Login)
		admin.POST("", controller.Admin.Create)
	}

	item := r.Group("/item")
	{
		item.Use(middleware.Jwt.Check)
		item.POST("", controller.Item.Create)
		item.GET("/:id", controller.Item.ReadCertain)
		item.GET("", controller.Item.Read)
	}
}

func main() {
	r := gin.New()

	if validate, ok := binding.Validator.Engine().(*validator.Validate); ok {
		validate.RegisterValidation("integer", util.Validator.Integer)
		validate.RegisterValidation("date", util.Validator.Date)
		validate.RegisterValidation("dateAfter", util.Validator.DateAfter)
	}

	r.Use(gin.Logger())

	gin.DisableConsoleColor()
	r.Use(middleware.Error.Exec())

	r.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{"PUT", "PATCH"},
		AllowHeaders:     []string{"Origin"},
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
	}))

	router(r)

	r.Run()
}
