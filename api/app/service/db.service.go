package service

import (
	"database/sql"
	"fmt"
	"ljoink/api-demo/util"

	_ "github.com/lib/pq"
)

type db struct{}

var connection *sql.DB

const (
	host     = "localhost"
	port     = 5432
	user     = "root"
	password = "pw"
	dbname   = "demo"
)

func (*db) GetConnection() *sql.DB {
	connStr := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)
	tmpDb, err := sql.Open("postgres", connStr)
	if err != nil {
		util.WriteLog(err.Error(), "error")
		panic(err.Error())
	}
	connection = tmpDb

	if err := connection.Ping(); err != nil {
		util.WriteLog(err.Error(), "error")
		panic(err.Error())
	}

	util.WriteLog("DB connected", "normal")

	return connection
}

func (db *db) PingWithReconnect() *sql.DB {
	if err := connection.Ping(); err != nil {
		util.WriteLog("Reconnect DB...", "error")
		connection = db.GetConnection()
	}
	return connection
}
